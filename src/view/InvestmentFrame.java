package view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import control.Controller;
import model.BankAccount;


public class InvestmentFrame extends JFrame{
	private static final int FRAME_WIDTH = 450;
	   private static final int FRAME_HEIGHT = 100;

	   private static final double DEFAULT_RATE = 5;   

	   private JLabel rateLabel;
	   private JTextField rateField;
	   private JButton button;
	   private JLabel resultLabel;
	   private JPanel panel;
	   private BankAccount account;
	   
	   
	   public InvestmentFrame(){  

	      // Use instance variables for components 
	      resultLabel = new JLabel("balance: ");

		   
	      // Use helper methods 
	      createTextField();
	      createButton();
	      createPanel();
	      
	      setVisible(true);
	      setSize(FRAME_WIDTH, FRAME_HEIGHT);
	   }

	   private void createTextField(){
	      rateLabel = new JLabel("Interest Rate: ");

	      final int FIELD_WIDTH = 10;
	      rateField = new JTextField(FIELD_WIDTH);
	      rateField.setText("" + DEFAULT_RATE);
	   }
	   
	   private void createButton(){
	      button = new JButton("Add Interest");
	   }

	   private void createPanel(){
	      panel = new JPanel();
	      panel.add(rateLabel);
	      panel.add(rateField);
	      panel.add(button);
	      panel.add(resultLabel);      
	      add(panel);
	   }

	public double getRate() {
		// TODO Auto-generated method stub
		return Double.parseDouble(rateField.getText());
	}
	
//	public void setResult(){
//		
//	}

	public void setResult(String result) {
		// TODO Auto-generated method stub
		resultLabel.setText("balance: " + result);
//		resultLabel = new JLabel("balance: " + account.getBalance());
	}

	public void setListener(ActionListener listener) {
		// TODO Auto-generated method stub
	      button.addActionListener(listener);     
	}
}
