package control;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.InvestmentFrame;
import model.BankAccount;

public class Controller {
	private BankAccount account;
	private InvestmentFrame view;
	private static final double INITIAL_BALANCE = 1000; 

	
	class AddInterestListener implements ActionListener{
       public void actionPerformed(ActionEvent event){
          double rate = view.getRate();
          double interest = account.calculateInterest(rate);
          account.deposit(interest);
          view.setResult(""+account.getBalance());
       }            
    }
    
    public Controller(){
    	this.account = new BankAccount(INITIAL_BALANCE);
    	this.view = new InvestmentFrame();
    	ActionListener listener = new AddInterestListener();
    	this.view.setListener(listener);
    	
    }
    
	public static void main(String[] args) {
		new Controller();
	}
}


/* ���ҧ���� Diagram �ͧ SampleGui  ��оԨ�ó���� ���ʷ������դ�������ѹ��ѹ������� 
 * ���� Controller �׷ʹ�ҡ���� InvestmentFrame��� ���� Bankaccount 
 * ��� ���� InvestmentFrame �׺�ʹ�ҡ����  Bankaccount ���� ActionListener ���  AddInterestListener 
 * 
 * �����¹�˵ؼ�� class controller ��� �����֧���͡��� object �ͧ bankaccount ��� InvestmentFrame �� attribute/local variable � class controller 
 * ���� ��ͧ������¡������ʹ�ͧ  bankaccount ��� InvestmentFrame ��
 */